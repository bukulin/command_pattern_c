#include "command.h"
#include "command_private.h"


void Command_Destroy(Command self)
{
	self->vtable->Destroy(self);
}

void Command_Run(Command self)
{
	self->vtable->Run(self);
}
