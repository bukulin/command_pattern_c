#ifndef COMMAND_H
#define COMMAND_H


typedef struct CommandStruct CommandStruct;
typedef CommandStruct* Command;

void Command_Destroy(Command self);
void Command_Run(Command self);

#endif
