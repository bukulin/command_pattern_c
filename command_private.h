#ifndef COMMAND_PRIVATE_H
#define COMMAND_PRIVATE_H

#include "command.h"

struct CommandInterfaceStruct
{
	void (*Destroy)(Command self);
	void (*Run)(Command self);
};
typedef struct CommandInterfaceStruct CommandInterfaceStruct;
typedef CommandInterfaceStruct* CommandInterface;

struct CommandStruct
{
	CommandInterface vtable;
};

#endif
