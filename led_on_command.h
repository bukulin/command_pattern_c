#ifndef LED_ON_COMMAND_H
#define LED_ON_COMMAND_H

#include "command.h"

Command LedOnCommand_Create(const int id);

#endif
