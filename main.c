#include "control.h"
#include "led.h"
#include "led_on_command.h"
#include <stddef.h>

int main()
{
	Command led3OnCommand = LedOnCommand_Create(3);
	Control_Create(led3OnCommand);
	Control_Run();
	Command_Destroy(led3OnCommand);
	led3OnCommand = NULL;
	return 0;
}
