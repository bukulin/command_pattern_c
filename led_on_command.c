#include "led_on_command.h"
#include "command_private.h"
#include "led.h"

#include <stdlib.h>

struct LedOnCommandStruct
{
	CommandStruct base;
	int id;
};
typedef struct LedOnCommandStruct LedOnCommandStruct;
typedef LedOnCommandStruct* LedOnCommand;

static void Destroy(Command base)
{
	LedOnCommand self = (LedOnCommand)base;
	free(self);
}
static void Run(Command base)
{
	LedOnCommand self = (LedOnCommand)base;
	Led_On(self->id);
}

static CommandInterfaceStruct interface = {
	.Destroy = Destroy,
	.Run = Run
};

Command LedOnCommand_Create(const int id)
{
	LedOnCommand self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;
	self->base.vtable = &interface;
	self->id = id;

	return (Command)self;
}
