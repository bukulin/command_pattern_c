#ifndef CONTROL_H
#define CONTROL_H

struct CommandStruct;

void Control_Create(struct CommandStruct* command);
void Control_Run(void);

#endif
