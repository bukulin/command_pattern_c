#include "control.h"
#include "command.h"
#include <stdio.h>

static Command commandToRun;

void Control_Create(Command command)
{
	commandToRun = command;
}

void Control_Run(void)
{
	printf("CONTROL: running\n");
	Command_Run(commandToRun);
}
